package com.test;

import java.util.List;

public class Employee {
	
	private int id;
	
	private String name;
	
	private String cmp;
	
	private List<String> skl;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCmp() {
		return cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}
	
	public List<String> getSkl() {
		return skl;
	}

	public void setSkl(List<String> skl) {
		this.skl = skl;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name+" Company : "+cmp);
		
		for(String str : skl)
		{
			System.out.println(str);
		}
	}
	
}
