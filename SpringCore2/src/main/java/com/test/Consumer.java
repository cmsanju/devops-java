package com.test;

import org.springframework.beans.factory.annotation.Autowired;

public class Consumer {
	
	private int id;
	
	private String name;
	
	private String type;
	
	@Autowired
	private Address adr;
	
	public Consumer()
	{
		
	}
	
	public Consumer(int id, String name, String type, Address adr)
	{
		this.id = id;
		this.name = name;
		this.type = type;
		
		//adr = new Address();
		this.adr = adr;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Address getAdr() {
		return adr;
	}

	public void setAdr(Address adr) {
		this.adr = adr;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name+" Type : "+type);
		
		adr.disp();
	}

}
