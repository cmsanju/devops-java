package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Consumer c = (Consumer)ctx.getBean("cns");
		
		c.disp();
		
		Consumer c1 = (Consumer)ctx.getBean("cns1");
		
		c1.disp();
	}

}
