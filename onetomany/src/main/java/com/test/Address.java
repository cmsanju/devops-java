package com.test;

import javax.persistence.*;

@Entity
@Table(name = "address")
public class Address {

	@Id
	@GeneratedValue
	private int id;
	private String street;
	private String city;
	@OneToOne(mappedBy = "address")
	private User user;

	public Address() {
	}

	public Address(int id, String street, String city) {
		this.id = id;
		this.street = street;
		this.city = city;
	}

	public String getstreet() {
		return street;
	}

	public void setstreet(String street) {
		this.street = street;
	}

	public String getcity() {
		return city;
	}

	public User getuser() {
		return user;
	}

	public void setuser(User user) {
		this.user = user;
	}

	public void setcity(String city) {
		this.city = city;
	}

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}
}
