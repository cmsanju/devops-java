package com.test;

import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class DisplayData {
	
	
	public static void main(String[] args) 
	{
		Configuration cfg = new Configuration();
		
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session = sf.openSession();
		
		Transaction t = session.beginTransaction();
		
		TypedQuery query=session.createQuery("from User");    
	    List<User> list=query.getResultList();    
	        
	    Iterator<User> itr=list.iterator();    
	    while(itr.hasNext()){    
	        User q=itr.next();    
	        System.out.println("User Name: "+q.getusername());    
	        
	        System.out.println(q.getaddress().getcity());
	        for (Vehicle  vl : q.getvehicle()) {
				System.out.println(vl.getname());
			}
	        for (Mobile ml : q.getmobile()) {
				System.out.println(ml.getbrand());
			}
	    }  
	    session.close();    
	    System.out.println("success");    
	}

}
