package com.test;

import java.util.*;

import javax.persistence.*;

@Entity
public class Mobile {
	@Id
	@GeneratedValue
	private int id;
	private String brand;
	private String model;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public String getbrand() {
		return brand;
	}

	public void setbrand(String brand) {
		this.brand = brand;
	}

	public String getmodel() {
		return model;
	}

	public void setmodel(String model) {
		this.model = model;
	}

	public User getuser() {
		return user;
	}

	public void setuser(User user) {
		this.user = user;
	}

}
