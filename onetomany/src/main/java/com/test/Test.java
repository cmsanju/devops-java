package com.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test {

	public static void main(String[] args) {
		User user = new User();
		User user2 = new User();
		user2.setusername("Alekya");
		user.setusername("VVSS");

		Address address = new Address();
		address.setstreet("ameerpet 15");
		address.setcity("Hyd");
		Address address2 = new Address();
		address2.setcity("Hyd");
		address2.setstreet("KPHB");

		Vehicle veh = new Vehicle();
		veh.setname("car");
		Vehicle vehicle = new Vehicle();
		vehicle.setname("jeep");
		Vehicle vehicle2 = new Vehicle();
		vehicle2.setname("bike");
		Vehicle vehicle3 = new Vehicle();
		vehicle3.setname("bus");
		Vehicle vehicle4 = new Vehicle();
		vehicle4.setname("cycle");
		Vehicle vehicle5 = new Vehicle();
		vehicle5.setname("truck");

		Mobile mobile = new Mobile();
		mobile.setbrand("sony");
		mobile.setmodel("xperia z3");
		Mobile mobile2 = new Mobile();
		mobile2.setbrand("redmi");
		mobile2.setmodel("note 5 pro");
		Mobile mobile3 = new Mobile();
		mobile3.setbrand("nokia");
		mobile3.setmodel("7 plus");

		user.setaddress(address);
		user2.setaddress(address2);
		address.setuser(user);
		address2.setuser(user2);

		user.getmobile().add(mobile);
		user.getmobile().add(mobile2);
		mobile.setuser(user);
		mobile2.setuser(user);
		user2.getmobile().add(mobile3);
		mobile3.setuser(user2);

		user.getvehicle().add(veh);
		user.getvehicle().add(vehicle);
		user.getvehicle().add(vehicle2);
		veh.getuser().add(user);
		vehicle.getuser().add(user);
		vehicle2.getuser().add(user);
		user2.getvehicle().add(vehicle3);
		user2.getvehicle().add(vehicle4);
		user2.getvehicle().add(vehicle5);
		vehicle3.getuser().add(user2);
		vehicle4.getuser().add(user2);
		vehicle5.getuser().add(user2);

		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		session.save(user);
		session.save(user2);
		session.getTransaction().commit();
		session.close();
	}
}
