package com.test;

import java.util.*;


import javax.persistence.*;
@Entity
@Table(name = "user_details")
public class User {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private int id;
	private String username;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "usr_vehicle", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns  = @JoinColumn(name = "vehicle_id"))
	private Collection<Vehicle> vehicle = new ArrayList<Vehicle>();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	private Address address;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_mobile_mapping", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "mobile_id"))
	private Collection<Mobile> mobile = new ArrayList<Mobile>();

	public Collection<Mobile> getmobile() {
		return mobile;
	}

	public void setmobile(Collection<Mobile> mobile) {
		this.mobile = mobile;
	}

	public Collection<Vehicle> getvehicle() {
		return vehicle;
	}

	public void setvehicle(Collection<Vehicle> vehicle) {
		this.vehicle = vehicle;
	}

	public Address getaddress() {
		return address;
	}

	public void setaddress(Address address) {
		this.address = address;
	}

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public String getusername() {
		return username;
	}

	public void setusername(String username) {
		this.username = username;
	}
}