package com.test;

import java.util.*;

import javax.persistence.*;

@Entity
public class Vehicle {

	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	private int id;
	private String name;

	@ManyToMany(mappedBy = "vehicle")
	private Collection<User> user = new ArrayList<User>();

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public Collection<User> getuser() {
		return user;
	}

public void setuser(Collection<User> user) {
this.user = user;
}
}
